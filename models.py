import mongoengine


WORKER_STATES = (
    ('new', 'new'),
    ('confirmed', 'confirmed'),
    ('banned', 'banned'),
    ('disabled', 'disabled')
)


class Channel(mongoengine.Document):
    tg_id = mongoengine.IntField(required=True)
    name = mongoengine.StringField()
    username = mongoengine.StringField(required=True)
    is_private = mongoengine.BooleanField(default=False)
    join_hash = mongoengine.StringField()
    worker = mongoengine.StringField()
    log = mongoengine.StringField()
    status = mongoengine.StringField()
    ban_history = mongoengine.ListField(mongoengine.StringField())


class User(mongoengine.Document):
    tg_id = mongoengine.IntField(required=True)
    username = mongoengine.StringField()
    full_name = mongoengine.StringField()
    is_bot = mongoengine.BooleanField(default=False)
    language_code = mongoengine.StringField()
    subscriptions = mongoengine.ListField(mongoengine.StringField())


class Worker(mongoengine.Document):
    tg_id = mongoengine.IntField()
    name = mongoengine.StringField()
    phone = mongoengine.StringField(required=True, regex='\++?\d+$|^$')
    state = mongoengine.StringField(choices=WORKER_STATES)
    subscribed_channels = mongoengine.ListField(mongoengine.StringField())
