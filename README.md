# Installation

1) Create if not exists module `feed_bot` inside your virtualenv's `site-packages`
2) Clone this repo and put inside `feed_bot` module

# Usage

```python
from feed_bot.storage import Channel, User, Worker

...

admin.add_view(ModelView(Channel, db))
admin.add_view(ModelView(User, db))
```